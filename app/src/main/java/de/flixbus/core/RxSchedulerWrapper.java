package de.flixbus.core;

import rx.Scheduler;

/**
 * Created by emanuele on 02.07.16.
 */
public interface RxSchedulerWrapper {
    Scheduler getObserveOn();
    Scheduler getSubscribeOn();
}
