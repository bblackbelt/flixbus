package de.flixbus.core;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by emanuele on 02.07.16.
 */
public class RxSchedulerWrapperImp implements RxSchedulerWrapper{

    @Override
    public Scheduler getObserveOn() {
        return AndroidSchedulers.mainThread();
    }

    @Override
    public Scheduler getSubscribeOn() {
        return Schedulers.io();
    }
}
