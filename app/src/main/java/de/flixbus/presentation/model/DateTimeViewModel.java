package de.flixbus.presentation.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by emanuele on 26.06.16.
 */
public class DateTimeViewModel implements ViewModel, Parcelable {
    private String tz;
    private long timestamp;

    public String getTz() {
        return tz;
    }

    public void setTz(String tz) {
        this.tz = tz;
    }

    public long getTimeStamp() {
        return timestamp;
    }

    public void setTimeStamp(long timestamp) {
        this.timestamp = timestamp;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.tz);
        dest.writeLong(this.timestamp);
    }

    public DateTimeViewModel() {
    }

    protected DateTimeViewModel(Parcel in) {
        this.tz = in.readString();
        this.timestamp = in.readLong();
    }

    public static final Creator<DateTimeViewModel> CREATOR = new Creator<DateTimeViewModel>() {
        @Override
        public DateTimeViewModel createFromParcel(Parcel source) {
            return new DateTimeViewModel(source);
        }

        @Override
        public DateTimeViewModel[] newArray(int size) {
            return new DateTimeViewModel[size];
        }
    };
}
