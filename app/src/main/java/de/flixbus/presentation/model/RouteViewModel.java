package de.flixbus.presentation.model;

/**
 * Created by emanuele on 26.06.16.
 */
public class RouteViewModel implements ViewModel {
    private int id;

    private String name;

    private String address;

    private String fullAddress;

    private CoordinatesViewModel coordinates;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public CoordinatesViewModel getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(CoordinatesViewModel coordinates) {
        this.coordinates = coordinates;
    }


    @Override
    public String toString() {
        return name;
    }
}
