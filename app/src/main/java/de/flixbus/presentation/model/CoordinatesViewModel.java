package de.flixbus.presentation.model;

/**
 * Created by emanuele on 26.06.16.
 */
public class CoordinatesViewModel implements ViewModel {

    private float latitude;
    private float longitude;

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
}
