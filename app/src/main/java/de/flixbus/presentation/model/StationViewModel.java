package de.flixbus.presentation.model;

/**
 * Created by emanuele on 26.06.16.
 */
public class StationViewModel implements ViewModel {
    public CoordinatesViewModel coordinates;
    public int id;
    public String name;
    public String address;
    public String full_address;

    public CoordinatesViewModel getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(CoordinatesViewModel coordinates) {
        this.coordinates = coordinates;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFullAddress() {
        return full_address;
    }

    public void setFullAddress(String full_address) {
        this.full_address = full_address;
    }

    @Override
    public String toString() {
        return name + " - " + full_address;
    }

}
