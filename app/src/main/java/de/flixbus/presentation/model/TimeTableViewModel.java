package de.flixbus.presentation.model;

import java.util.List;

/**
 * Created by emanuele on 26.06.16.
 */
public class TimeTableViewModel implements ViewModel {

    private DateTimeViewModel mDateTime;
    private List<TripViewModel> mArrivals;
    private List<TripViewModel> mDepartures;
    private StationViewModel mStation;
    private String message;

    public DateTimeViewModel getDateTime() {
        return mDateTime;
    }

    public void setDateTime(DateTimeViewModel mDateTime) {
        this.mDateTime = mDateTime;
    }

    public List<TripViewModel> getArrivals() {
        return mArrivals;
    }

    public void setArrivals(List<TripViewModel> mArrivals) {
        this.mArrivals = mArrivals;
    }

    public List<TripViewModel> getDepartures() {
        return mDepartures;
    }

    public void setDepartures(List<TripViewModel> mDepartures) {
        this.mDepartures = mDepartures;
    }

    public StationViewModel getStation() {
        return mStation;
    }

    public void setStation(StationViewModel mStation) {
        this.mStation = mStation;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
