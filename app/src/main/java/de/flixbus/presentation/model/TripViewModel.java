package de.flixbus.presentation.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emanuele on 26.06.16.
 */
public class TripViewModel implements ViewModel, Parcelable {

    private String through_the_stations;
    private String line_direction;
    private List<RouteViewModel> route;
    private String direction;
    private String line_code;
    private DateTimeViewModel datetime;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.through_the_stations);
        dest.writeString(this.line_direction);
        dest.writeList(this.route);
        dest.writeString(this.direction);
        dest.writeString(this.line_code);
        dest.writeParcelable(this.datetime, flags);
    }

    public TripViewModel() {
    }

    protected TripViewModel(Parcel in) {
        this.through_the_stations = in.readString();
        this.line_direction = in.readString();
        this.route = new ArrayList<RouteViewModel>();
        in.readList(this.route, RouteViewModel.class.getClassLoader());
        this.direction = in.readString();
        this.line_code = in.readString();
        this.datetime = in.readParcelable(DateTimeViewModel.class.getClassLoader());
    }

    public static final Parcelable.Creator<TripViewModel> CREATOR = new Parcelable.Creator<TripViewModel>() {
        @Override
        public TripViewModel createFromParcel(Parcel source) {
            return new TripViewModel(source);
        }

        @Override
        public TripViewModel[] newArray(int size) {
            return new TripViewModel[size];
        }
    };

    public String getThrough_the_stations() {
        return through_the_stations;
    }

    public void setThroughTheStations(String through_the_stations) {
        this.through_the_stations = through_the_stations;
    }

    public String getLineDirection() {
        return line_direction;
    }

    public void setLineDirection(String line_direction) {
        this.line_direction = line_direction;
    }

    public List<RouteViewModel> getRoute() {
        return route;
    }

    public void setRoute(List<RouteViewModel> route) {
        this.route = route;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getLineCode() {
        return line_code;
    }

    public void setLineCode(String line_code) {
        this.line_code = line_code;
    }

    public DateTimeViewModel getDateTime() {
        return datetime;
    }

    public void setDatetime(DateTimeViewModel datetime) {
        this.datetime = datetime;
    }
}
