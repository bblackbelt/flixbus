package de.flixbus.presentation.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import de.flixbus.R;
import de.flixbus.presentation.adapter.ArrivalsDeparturesAdapter;
import de.flixbus.presentation.model.TimeTableViewModel;
import de.flixbus.presentation.widgets.EmptyViewRecyclerView;

/**
 * Created by emanuele on 22.06.16.
 */
public class ArrivalDeparturesListFragment<T extends TimeTableViewModel> extends Fragment {

    private final static int DEPARTURES = 0;
    private final static int ARRIVALS = 1;

    private ArrivalsDeparturesAdapter mArrivalDeparturesAdapter;
    private List<T> mData;
    private RecyclerView mRecyclerView;
    private TimeTableViewModel mTimeTable;
    private View mArrivalView;
    private View mDeparturesView;
    private int mCurrentDataType = DEPARTURES;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recyclerview_fragment_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        ((EmptyViewRecyclerView) mRecyclerView).setEmptyView(view.findViewById(R.id.empty));
        mArrivalDeparturesAdapter = new ArrivalsDeparturesAdapter();
        mArrivalDeparturesAdapter.setData(mTimeTable != null ? mTimeTable.getDepartures() : null);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mArrivalDeparturesAdapter);
        mRecyclerView.setHasFixedSize(true);
        mArrivalView = view.findViewById(R.id.arrivals);
        mArrivalView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleData(ARRIVALS);
            }
        });

        mDeparturesView = view.findViewById(R.id.departures);
        mDeparturesView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleData(DEPARTURES);
            }
        });
        mDeparturesView.setSelected(true);
    }

    private void toggleData(int type) {
        if (mCurrentDataType == type) {
            return;
        }
        mArrivalDeparturesAdapter.setData(type == DEPARTURES ? mTimeTable.getDepartures() : mTimeTable.getArrivals());
        mArrivalView.setSelected(type == ARRIVALS);
        mDeparturesView.setSelected(type == DEPARTURES);
        mCurrentDataType = type;
    }

    public void setTimeTable(TimeTableViewModel timeTable) {
        mTimeTable = timeTable;
    }

}
