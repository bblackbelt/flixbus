package de.flixbus.presentation.fragments;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import de.flixbus.presentation.model.TripViewModel;
import de.flixbus.presentation.view.TimeTableView;

/**
 * Created by emanuele on 24.06.16.
 */
public abstract class BaseTimeTableFragment extends Fragment implements TimeTableView {


    @Override
    public void showTrip(TripViewModel trip) {
    }


    @Override
    public void showError(String message) {
        showMessage(message);
    }

    protected void showMessage(String message) {
        Snackbar.make(getView() != null
                ? getView()
                : getActivity().findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show();
    }


    protected void replaceChildFragment(int containerId, Fragment fragment, String tag, String removeTag) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        Fragment oldFragment = getChildFragmentManager().findFragmentByTag(removeTag);
        if (oldFragment != null) {
            transaction.remove(oldFragment);
        }
        transaction.commit();
        addChildFragment(containerId, fragment, tag);
    }

    protected void addChildFragment(int containerId, Fragment fragment, String tag) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(containerId, fragment, tag);
        transaction.commit();
    }
}
