package de.flixbus.presentation.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.flixbus.R;
import de.flixbus.core.RxSchedulerWrapperImp;
import de.flixbus.data.entity.TimeTable;
import de.flixbus.data.interactor.TimeTableInteractor;
import de.flixbus.presentation.activiities.TimeTableListener;
import de.flixbus.presentation.adapter.ArrivalDeparturesViewPagerAdapter;
import de.flixbus.presentation.model.TimeTableViewModel;
import de.flixbus.presentation.presenter.TimeTablePresenter;

/**
 * Created by emanuele on 25.06.16.
 */
public class TimeTableFragment extends BaseTimeTableFragment {

    public final static String STATION_ID_KEY = "STATION_ID_KEY";
    public static final int BERLIN_STATION_ID = 1;
    public static final int MUNICH_STATION_ID = 10;

    private final static String LOG_TAG = TimeTableFragment.class.getSimpleName();

    private TimeTablePresenter mPresenter;
    private Collection<TimeTableViewModel> mCurrentTimeTable;

    private TimeTable data;

    private TimeTableListener mTimeTableListener;
    private int mCurrentStation = BERLIN_STATION_ID;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public TimeTableFragment() {
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TimeTableListener) {
            mTimeTableListener = (TimeTableListener) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.time_table_fragment_container, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        final int stationId = getArguments() == null ? BERLIN_STATION_ID : getArguments().getInt(STATION_ID_KEY);
        mPresenter = new TimeTablePresenter(new TimeTableInteractor(
                new RxSchedulerWrapperImp(),
                stationId));
        mPresenter.setView(this);
        mPresenter.loadTimeTable();
        setupViewPager(new ArrayList<TimeTableViewModel>());
    }

    private void setupViewPager(List<TimeTableViewModel> data) {
        if (getView() == null) {
            return;
        }
        ViewPager viewPager = (ViewPager) getView().findViewById(R.id.arr_dep_container);
        viewPager.setAdapter(new ArrivalDeparturesViewPagerAdapter(getChildFragmentManager(), data));

        TabLayout tabLayout = (TabLayout) getView().findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }


    @Override
    public void onTimeTableReady(Collection<TimeTableViewModel> data) {
        mCurrentTimeTable = data;
        List<TimeTableViewModel> list = new ArrayList<>(data);
        setupViewPager(list);
        if (!list.isEmpty() && list.get(0) != null && mTimeTableListener != null) {
            mTimeTableListener.onDataReceived(list.get(0).getStation());
        }
    }

    @Override
    public void setProgressIndicator(boolean active) {
        setLoadingIndicatorVisibility(active);
    }

    private void setLoadingIndicatorVisibility(boolean active) {
        final View view;
        if ((view = getView()) == null) {
            return;
        }
        if (view.findViewById(R.id.loader_container) != null) {
            view.findViewById(R.id.loader_container).setVisibility(active ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.destroy();
        mPresenter = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        menu.findItem(R.id.berlin).setChecked(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.berlin:
                if (mCurrentStation == BERLIN_STATION_ID) {
                    return true;
                }
                mCurrentStation = BERLIN_STATION_ID;
                break;
            case R.id.munich:
                if (mCurrentStation == MUNICH_STATION_ID) {
                    return true;
                }
                mCurrentStation = MUNICH_STATION_ID;
                break;
            default:
                break;
        }
        mPresenter.unsubscribe();
        mPresenter.setInteractor(new TimeTableInteractor(
                new RxSchedulerWrapperImp(),
                mCurrentStation));
        mPresenter.loadTimeTable();
        return super.onOptionsItemSelected(item);
    }
}
