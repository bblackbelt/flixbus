package de.flixbus.presentation.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.flixbus.data.entity.Trip;
import de.flixbus.presentation.model.TripViewModel;

/**
 * Created by emanuele on 26.06.16.
 */
public class TripMapper extends BasicMapper<Trip, TripViewModel> {
    @Override
    public TripViewModel transform(Trip entity) {
        if (entity == null) {
            return null;
        }
        DateTimeMapper dateTimeMapper = new DateTimeMapper();
        TripViewModel tripViewModel = new TripViewModel();
        tripViewModel.setDatetime(dateTimeMapper.transform(entity.getDateTime()));
        tripViewModel.setDirection(entity.getDirection());
        tripViewModel.setLineCode(entity.getLineCode());
        tripViewModel.setLineDirection(entity.getLineDirection());
        tripViewModel.setThroughTheStations(entity.getThroughTheStations());
        tripViewModel.setRoute(new ArrayList<>(new RouteMapper().transform(entity.getRoute())));
        return tripViewModel;
    }
}
