package de.flixbus.presentation.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.flixbus.data.entity.Route;
import de.flixbus.presentation.model.RouteViewModel;

/**
 * Created by emanuele on 26.06.16.
 */
public class RouteMapper extends BasicMapper<Route, RouteViewModel> {

    @Override
    public RouteViewModel transform(Route entity) {
        if (entity == null) {
            return null;
        }
        RouteViewModel routeViewModel = new RouteViewModel();
        routeViewModel.setAddress(entity.getAddress());
        routeViewModel.setFullAddress(entity.getFullAddress());
        routeViewModel.setId(routeViewModel.getId());
        routeViewModel.setName(entity.getName());
        routeViewModel.setCoordinates(new CoordinatesMapper().transform(entity.getCoordinates()));
        return routeViewModel;
    }
}
