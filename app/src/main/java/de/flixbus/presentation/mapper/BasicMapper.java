package de.flixbus.presentation.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.flixbus.data.entity.Entity;
import de.flixbus.presentation.model.ViewModel;

/**
 * Created by emanuele on 02.07.16.
 */
public abstract class BasicMapper<T extends Entity, E extends ViewModel> implements Mapper<T, E> {

    @Override
    public Collection<E> transform(Collection<T> entities) {
        List<E> list = new ArrayList<>();
        for (T t : entities) {
            list.add(transform(t));
        }
        return list;
    }
}
