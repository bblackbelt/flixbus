package de.flixbus.presentation.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.flixbus.data.entity.TimeTable;
import de.flixbus.presentation.DateUtils;
import de.flixbus.presentation.model.TimeTableViewModel;
import de.flixbus.presentation.model.TripViewModel;
import de.flixbus.presentation.view.TimeTableView;

/**
 * Created by emanuele on 26.06.16.
 */
public class TimeTableMapper implements Mapper<TimeTable, TimeTableViewModel> {


    @Override
    public TimeTableViewModel transform(TimeTable entity) {
        TimeTableViewModel timeTableViewModel = new TimeTableViewModel();
        TripMapper tripMapper = new TripMapper();
        timeTableViewModel.setArrivals(new ArrayList<>(tripMapper.transform(entity.getArrivals())));
        timeTableViewModel.setDepartures(new ArrayList<>(tripMapper.transform(entity.getDepartures())));
        timeTableViewModel.setStation(new StationMapper().transform(entity.getStation()));
        timeTableViewModel.setMessage(entity.getMessage());
        return timeTableViewModel;
    }

    private List<TimeTableViewModel> transformInternal(TimeTable timeTable) {
        if ((timeTable.getArrivals() == null || timeTable.getArrivals().isEmpty()) &&
                (timeTable.getDepartures() == null || timeTable.getDepartures().isEmpty())) {
            List<TimeTableViewModel> timeTableViewList = new ArrayList<>();
            timeTableViewList.add(transform(timeTable));
            return timeTableViewList;
        }

        TripMapper tripMapper = new TripMapper();
        List<TripViewModel> arrivals = new ArrayList<>(tripMapper.transform(timeTable.getArrivals()));
        List<TripViewModel> tmpTripViewModel = null;
        Map<String, TimeTableViewModel> timeTableViews = new HashMap<>();

        TimeTableViewModel currentTimeTableView;
        String currentDate = null;
        for (TripViewModel tripViewModel : arrivals) {
            String tDate = DateUtils.getFormattedDate(tripViewModel.getDateTime());
            if (!tDate.equals(currentDate) || timeTableViews.get(tDate) == null) {
                currentTimeTableView = new TimeTableViewModel();
                currentTimeTableView.setDateTime(tripViewModel.getDateTime());
                currentTimeTableView.setArrivals(tmpTripViewModel = new ArrayList<>());
                currentTimeTableView.setStation(new StationMapper().transform(timeTable.getStation()));
                currentTimeTableView.setMessage(timeTable.getMessage());
                timeTableViews.put(tDate, currentTimeTableView);
                currentDate = tDate;
            }
            tmpTripViewModel.add(tripViewModel);
        }

        List<TripViewModel> departures = new ArrayList<>(tripMapper.transform(timeTable.getDepartures()));
        tmpTripViewModel = null;
        currentDate = null;
        for (TripViewModel tripViewModel : departures) {
            String tDate = DateUtils.getFormattedDate(tripViewModel.getDateTime());
            if (timeTableViews.get(tDate) == null) {
                currentTimeTableView = new TimeTableViewModel();
                currentTimeTableView.setArrivals(tmpTripViewModel = new ArrayList<>());
                currentTimeTableView.setDateTime(tripViewModel.getDateTime());
                currentTimeTableView.setStation(new StationMapper().transform(timeTable.getStation()));
                currentTimeTableView.setMessage(timeTable.getMessage());
                timeTableViews.put(tDate, currentTimeTableView);
            } else {
                if (!tDate.equals(currentDate)) {
                    timeTableViews.get(tDate).setDepartures(tmpTripViewModel = new ArrayList<>());
                    currentDate = DateUtils.getFormattedDate(timeTableViews.get(tDate).getDateTime());
                }
            }
            tmpTripViewModel.add(tripViewModel);
        }
        return new ArrayList<>(timeTableViews.values());
    }

    @Override
    public Collection<TimeTableViewModel> transform(Collection<TimeTable> entities) {
        List<TimeTableViewModel> timeTableViews = new ArrayList<>();
        for (TimeTable timeTable : entities) {
            timeTableViews.addAll(transformInternal(timeTable));
        }
        return timeTableViews;
    }
}
