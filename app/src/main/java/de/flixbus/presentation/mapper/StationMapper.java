package de.flixbus.presentation.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.flixbus.data.entity.Station;
import de.flixbus.presentation.model.StationViewModel;

/**
 * Created by emanuele on 26.06.16.
 */
public class StationMapper extends BasicMapper<Station, StationViewModel> {
    @Override
    public StationViewModel transform(Station entity) {
        if (entity == null) {
            return null;
        }
        StationViewModel stationViewModel = new StationViewModel();
        stationViewModel.setFullAddress(entity.getFullAddress());
        stationViewModel.setId(entity.getId());
        stationViewModel.setAddress(entity.getAddress());
        stationViewModel.setCoordinates(new CoordinatesMapper().transform(entity.getCoordinates()));
        stationViewModel.setName(entity.getName());
        return stationViewModel;
    }
}
