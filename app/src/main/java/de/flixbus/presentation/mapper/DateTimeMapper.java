package de.flixbus.presentation.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.flixbus.data.entity.Trip;
import de.flixbus.presentation.model.DateTimeViewModel;

/**
 * Created by emanuele on 26.06.16.
 */
public class DateTimeMapper extends BasicMapper<Trip.DateTime, DateTimeViewModel> {
    @Override
    public DateTimeViewModel transform(Trip.DateTime entity) {
        if (entity == null) {
            return null;
        }
        DateTimeViewModel dateTimeViewModel = new DateTimeViewModel();
        dateTimeViewModel.setTimeStamp(entity.getTimeStamp() * 1000);
        dateTimeViewModel.setTz(entity.getTz());
        return dateTimeViewModel;
    }
}
