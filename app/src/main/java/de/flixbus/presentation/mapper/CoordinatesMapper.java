package de.flixbus.presentation.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.flixbus.data.entity.Coordinates;
import de.flixbus.presentation.model.CoordinatesViewModel;

/**
 * Created by emanuele on 26.06.16.
 */
public class CoordinatesMapper extends BasicMapper<Coordinates, CoordinatesViewModel> {
    @Override
    public CoordinatesViewModel transform(Coordinates entity) {
        if (entity == null) {
            return null;
        }
        CoordinatesViewModel c = new CoordinatesViewModel();
        c.setLatitude(entity.getLatitude());
        c.setLongitude(entity.getLongitude());
        return c;
    }
}
