package de.flixbus.presentation.mapper;

import java.util.Collection;

import de.flixbus.data.entity.Entity;
import de.flixbus.presentation.model.ViewModel;

/**
 * Created by emanuele on 26.06.16.
 */
public interface Mapper<T extends Entity, E extends ViewModel> {

    E transform(T entity);

    Collection<E> transform(Collection<T> entities);

}
