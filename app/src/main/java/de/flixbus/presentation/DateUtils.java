package de.flixbus.presentation;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import de.flixbus.presentation.model.DateTimeViewModel;

/**
 * Created by emanuele on 25.06.16.
 */
public class DateUtils {

    private static final SimpleDateFormat sDateFormat = new SimpleDateFormat("MMMM d, yyyy", Locale.GERMAN);
    private static final SimpleDateFormat sTimeFormat = new SimpleDateFormat("HH:mm", Locale.GERMAN);

    public static String getTodayDate() {
        return getFormattedDate(0);
    }

    public static String getFormattedDate(int daysToAdd) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, daysToAdd);
        return sDateFormat.format(calendar.getTime());
    }

    public static String getFormattedDate(DateTimeViewModel dateTime) {
        if (dateTime == null) {
            return "";
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateTime.getTimeStamp());
        calendar.setTimeZone(TimeZone.getTimeZone(dateTime.getTz()));
        return sDateFormat.format(calendar.getTime());
    }

    public static String getTime(DateTimeViewModel dateTime) {
        if (dateTime == null) {
            return "";
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateTime.getTimeStamp());
        calendar.setTimeZone(TimeZone.getTimeZone(dateTime.getTz()));
        return sTimeFormat.format(calendar.getTime());
    }

    private DateUtils() {
    }
}
