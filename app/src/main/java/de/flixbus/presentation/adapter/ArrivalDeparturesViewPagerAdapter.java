package de.flixbus.presentation.adapter;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import de.flixbus.presentation.DateUtils;
import de.flixbus.presentation.fragments.ArrivalDeparturesListFragment;
import de.flixbus.presentation.model.TimeTableViewModel;

/**
 * Created by emanuele on 26.06.16.
 */
public class ArrivalDeparturesViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<TimeTableViewModel> mDataSet;

    public ArrivalDeparturesViewPagerAdapter(FragmentManager fm, List<TimeTableViewModel> dataSet) {
        super(fm);
        mDataSet = dataSet;
    }

    @Override
    public Fragment getItem(int position) {
        ArrivalDeparturesListFragment fragment = new ArrivalDeparturesListFragment();
        fragment.setTimeTable(mDataSet.get(position));
        return fragment;
    }

    @Override
    public int getCount() {
        return mDataSet == null ? 0 : mDataSet.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (mDataSet == null || mDataSet.isEmpty()) {
            return null;
        }
        return DateUtils.getFormattedDate(mDataSet.get(position).getDateTime());
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }
}
