package de.flixbus.presentation.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import de.flixbus.presentation.model.TripViewModel;

/**
 * Created by emanuele on 26.06.16.
 */
public abstract class BaseTimeTableViewHolder extends RecyclerView.ViewHolder {

    public BaseTimeTableViewHolder(View itemView) {
        super(itemView);
    }

    abstract void drawTrip(TripViewModel trip);
}