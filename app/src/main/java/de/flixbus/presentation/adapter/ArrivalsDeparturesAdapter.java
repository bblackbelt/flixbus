package de.flixbus.presentation.adapter;

import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import de.flixbus.R;
import de.flixbus.presentation.DateUtils;
import de.flixbus.presentation.model.TripViewModel;

/**
 * Created by emanuele on 22.06.16.
 */
public class ArrivalsDeparturesAdapter extends RecyclerView.Adapter<BaseTimeTableViewHolder> {


    public static class TimeTableViewHolder extends BaseTimeTableViewHolder implements View.OnClickListener {

        public TextView mRoute;
        public TextView mRouteDirection;
        public TextView mRouteTime;


        private TripViewModel mTrip;

        public TimeTableViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mRoute = (TextView) itemView.findViewById(R.id.route);
            mRouteDirection = (TextView) itemView.findViewById(R.id.route_direction);
            mRouteTime = (TextView) itemView.findViewById(R.id.route_time);
        }

        @Override
        void drawTrip(TripViewModel trip) {
            mTrip = trip;
            mRoute.setText(trip.getLineCode());
            mRouteDirection.setText(trip.getLineDirection());
            mRouteTime.setText(DateUtils.getTime(trip.getDateTime()));
        }

        @Override
        public void onClick(View v) {
            AlertDialog alertDialog = new AlertDialog.Builder(v.getContext())
                    .setTitle(mTrip.getLineCode())
                    .setAdapter(new ArrayAdapter<>(v.getContext(),
                            android.R.layout.simple_list_item_1, mTrip.getRoute()), null)
                    .create();
            alertDialog.show();
        }
    }

    private List<TripViewModel> mDataSet;

    @Override
    public BaseTimeTableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TimeTableViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.time_table_item, parent, false));
    }

    @Override
    public void onBindViewHolder(BaseTimeTableViewHolder holder, int position) {
        holder.drawTrip(mDataSet.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataSet == null ? 0 : mDataSet.size();
    }

    public void setData(List<TripViewModel> dataSet) {
        mDataSet = dataSet;
        notifyDataSetChanged();
    }
}

