package de.flixbus.presentation.activiities;

import de.flixbus.presentation.model.StationViewModel;

/**
 * Created by emanuele on 25.06.16.
 */
public interface TimeTableListener {
    void onDataReceived(StationViewModel data);
}
