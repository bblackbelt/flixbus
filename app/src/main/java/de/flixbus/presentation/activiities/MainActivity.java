package de.flixbus.presentation.activiities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import de.flixbus.R;
import de.flixbus.presentation.fragments.TimeTableFragment;
import de.flixbus.presentation.model.StationViewModel;

public class MainActivity extends AppCompatActivity implements TimeTableListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.container, new TimeTableFragment());
            transaction.commit();
        }
        getSupportActionBar().setTitle(null);
    }


    @Override
    public void onDataReceived(StationViewModel data) {
        if (data == null) {
            return;
        }
        getSupportActionBar().setTitle(data.getName());
    }
}
