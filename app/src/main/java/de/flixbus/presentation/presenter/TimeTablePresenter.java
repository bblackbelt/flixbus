package de.flixbus.presentation.presenter;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.flixbus.data.entity.TimeTable;
import de.flixbus.data.interactor.DefaultSubscriber;
import de.flixbus.data.interactor.Interactor;
import de.flixbus.presentation.mapper.TimeTableMapper;
import de.flixbus.presentation.model.TimeTableViewModel;
import de.flixbus.presentation.view.TimeTableView;

/**
 * Created by emanuele on 24.06.16.
 */
public class TimeTablePresenter implements Presenter {


    private final class TimeTableSubscriber extends DefaultSubscriber<TimeTable> {

        @Override
        public void onCompleted() {
            hideLoadingView();
        }

        @Override
        public void onError(Throwable e) {
            hideLoadingView();
            showErrorMessage(e);
        }

        @Override
        public void onNext(TimeTable timeTable) {
            showTimeTable(timeTable);
        }
    }

    private TimeTableView mTimeTableView;
    private Interactor mInteractor;

    public TimeTablePresenter(Interactor interactor) {
        mInteractor = interactor;
    }

    public void setInteractor(Interactor interactor) {
        mInteractor = interactor;
    }

    public void setView(@NonNull TimeTableView view) {
        mTimeTableView = view;
    }

    public void loadTimeTable() {
        showLoadingView();
        fetchData();
    }

    private void showLoadingView() {
        if (mTimeTableView == null) {
            return;
        }
        mTimeTableView.setProgressIndicator(true);
    }

    private void hideLoadingView() {
        if (mTimeTableView == null) {
            return;
        }
        mTimeTableView.setProgressIndicator(false);
    }

    private void showTimeTable(final TimeTable timeTable) {
        if (mTimeTableView == null) {
            return;
        }
        TimeTableMapper timeTableMapper = new TimeTableMapper();
        List<TimeTable> timeTables = new ArrayList<>();
        timeTables.add(timeTable);
        Collection<TimeTableViewModel> timeTablesViewModel = timeTableMapper.transform(timeTables);
        mTimeTableView.onTimeTableReady(timeTablesViewModel);
    }


    private void showErrorMessage(Throwable e) {
        if (mTimeTableView == null) {
            return;
        }
        mTimeTableView.showError(TextUtils.isEmpty(e.getMessage()) ? "Error fetching the time table" : e.getMessage());
    }


    private void fetchData() {
        mInteractor.execute(new TimeTableSubscriber());
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void unsubscribe() {
        if (mInteractor != null) {
            mInteractor.unsubscribe();
        }
    }

    @Override
    public void destroy() {
        unsubscribe();
        mTimeTableView = null;
    }
}
