package de.flixbus.presentation.presenter;

/**
 * Created by emanuele on 24.06.16.
 */
public interface Presenter {
    void resume();

    void pause();

    void destroy();

    void unsubscribe();
}
