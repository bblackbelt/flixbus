package de.flixbus.presentation.view;

/**
 * Created by emanuele on 24.06.16.
 */

public interface LoadDataView {

    void setProgressIndicator(boolean active);


    void showError(String message);

}