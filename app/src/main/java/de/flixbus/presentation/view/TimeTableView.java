package de.flixbus.presentation.view;

import java.util.Collection;

import de.flixbus.presentation.view.LoadDataView;
import de.flixbus.presentation.model.TimeTableViewModel;
import de.flixbus.presentation.model.TripViewModel;

/**
 * Created by emanuele on 24.06.16.
 */
public interface TimeTableView extends LoadDataView {

    void onTimeTableReady(Collection<TimeTableViewModel> data);

    void showTrip(TripViewModel trip);
}
