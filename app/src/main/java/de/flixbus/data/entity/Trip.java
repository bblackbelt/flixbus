package de.flixbus.data.entity;

import java.util.List;

/**
 * Created by emanuele on 22.06.16.
 */
public class Trip implements Entity {

    public static class DateTime implements Entity {
        private String tz;
        private long timestamp;

        public String getTz() {
            return tz;
        }

        public void setTz(String tz) {
            this.tz = tz;
        }

        public long getTimeStamp() {
            return timestamp;
        }

        public void setTimeStamp(long timestamp) {
            this.timestamp = timestamp;
        }

    }

    private String through_the_stations;
    private String line_direction;
    private List<Route> route;
    private String direction;
    private String line_code;
    private DateTime datetime;


    public String getThroughTheStations() {
        return through_the_stations;
    }

    public void setThroughTheStations(String through_the_stations) {
        this.through_the_stations = through_the_stations;
    }

    public String getLineDirection() {
        return line_direction;
    }

    public void setLineDirection(String line_direction) {
        this.line_direction = line_direction;
    }

    public List<Route> getRoute() {
        return route;
    }

    public void setRoute(List<Route> route) {
        this.route = route;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getLineCode() {
        return line_code;
    }

    public void setLineCode(String line_code) {
        this.line_code = line_code;
    }

    public DateTime getDateTime() {
        return datetime;
    }

    public void setDatetime(DateTime datetime) {
        this.datetime = datetime;
    }

}
