package de.flixbus.data.entity;

import java.util.List;

public class TimeTable implements Entity {


    public List<Trip> arrivals;
    public List<Trip> departures;
    public String message;
    public Station station;

    @Override
    public String toString() {
        if (station != null) {
            return station.toString();
        }
        return super.toString();
    }

    public List<Trip> getArrivals() {
        return arrivals;
    }

    public void setArrivals(List<Trip> arrivals) {
        this.arrivals = arrivals;
    }

    public List<Trip> getDepartures() {
        return departures;
    }

    public void setDepartures(List<Trip> departures) {
        this.departures = departures;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }
}
