package de.flixbus.data.entity;

/**
 * Created by emanuele on 26.06.16.
 */
public class Station implements Entity {

    private Coordinates coordinates;
    private int id;
    private String name;
    private String address;
    private String full_address;

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFullAddress() {
        return full_address;
    }

    public void setFullAddress(String full_address) {
        this.full_address = full_address;
    }

    @Override
    public String toString() {
        return name + " - " + full_address;
    }

}
