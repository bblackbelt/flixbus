package de.flixbus.data.interactor;

import de.flixbus.core.RxSchedulerWrapper;
import de.flixbus.data.entity.TimeTable;
import de.flixbus.data.net.RestClient;
import rx.Observable;

/**
 * Created by emanuele on 24.06.16.
 */
public class TimeTableInteractor extends Interactor<TimeTable> {

    private final int mStationId;


    public TimeTableInteractor(RxSchedulerWrapper schedulerWrapper, int stationId) {
        super(schedulerWrapper);
        mStationId = stationId;
    }


    @Override
    public Observable<TimeTable> buildInteractorObservable() {
        return RestClient.getInstance().getTimeTable(mStationId);
    }


}
