package de.flixbus.data.interactor;

import de.flixbus.core.RxSchedulerWrapper;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by emanuele on 24.06.16.
 */
public abstract class Interactor<T> {

    private Subscription mSubscription = Subscriptions.empty();
    private RxSchedulerWrapper mSchedulerWrapper;

    public Interactor(RxSchedulerWrapper schedulerWrapper) {
        mSchedulerWrapper = schedulerWrapper;
    }

    public void unsubscribe() {
        if (mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }

    public void execute(Subscriber<T> subscriber) {
        mSubscription = buildInteractorObservable()
                .subscribeOn(mSchedulerWrapper.getSubscribeOn())
                .observeOn(mSchedulerWrapper.getObserveOn())
                .subscribe(subscriber);
    }

    protected abstract Observable<T> buildInteractorObservable();
}
