package de.flixbus.data.net;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import de.flixbus.data.entity.Station;
import de.flixbus.data.entity.TimeTable;
import de.flixbus.data.entity.Trip;

/**
 * Created by emanuele on 22.06.16.
 */
public class TimeTableDeserializer implements JsonDeserializer<TimeTable> {
    @Override
    public TimeTable deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        TimeTable timeTable = new TimeTable();

        JsonObject root = json.getAsJsonObject();
        if (!root.has("timetable")) {
            return timeTable;
        }
        JsonObject timeTableJson = root.getAsJsonObject("timetable");
        Type listType = null;
        if (timeTableJson.has("arrivals")) {
            listType = new TypeToken<List<Trip>>() {
            }.getType();
            timeTable.arrivals = context.deserialize(timeTableJson.getAsJsonArray("arrivals"), listType);
        }
        if (timeTableJson.has("departures")) {
            listType = new TypeToken<List<Trip>>() {
            }.getType();
            timeTable.departures = context.deserialize(timeTableJson.getAsJsonArray("departures"), listType);
        }
        if (timeTableJson.has("message")) {
            timeTable.message = timeTableJson.get("message").getAsString();
        }
        if (root.has("station")) {
            timeTable.station = context.deserialize(root.getAsJsonObject("station"), Station.class);
        }
        return timeTable;
    }
}
