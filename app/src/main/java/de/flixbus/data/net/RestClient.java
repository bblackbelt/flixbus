package de.flixbus.data.net;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import de.flixbus.data.entity.TimeTable;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by emanuele on 22.06.16.
 */
public class RestClient {

    public interface FlixBusApiService {
        @GET("network/station/{station_id}/timetable")
        Observable<TimeTable> getTimeTable(@Path("station_id") int stationId);
    }

    private class AuthenticatorInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            return chain.proceed(request.newBuilder()
                    .addHeader("Accept", " application/json")
                    .addHeader("X-Api-Authentication", "intervIEW_TOK3n").build());
        }
    }


    private static final String BASE_URL = "http://api.mobile.staging.mfb.io/mobile/v1/";


    private final FlixBusApiService mFlixBusApiService;

    private static RestClient sRestClient;

    private RestClient() {

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new AuthenticatorInterceptor())
                .build();

        Gson gson = new GsonBuilder().registerTypeAdapter(TimeTable.class, new TimeTableDeserializer()).create();

        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        mFlixBusApiService = restAdapter.create(FlixBusApiService.class);
    }

    public static synchronized RestClient getInstance() {
        if (sRestClient == null) {
            sRestClient = new RestClient();
        }
        return sRestClient;
    }

    public Observable<TimeTable> getTimeTable(final int stationId) {
        return mFlixBusApiService.getTimeTable(stationId);
    }

    public FlixBusApiService getFlixBusApiService() {
        return mFlixBusApiService;
    }
}
