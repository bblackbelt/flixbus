package de.flixbus.data.entity;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.flixbus.presentation.mapper.TimeTableMapper;
import de.flixbus.presentation.model.StationViewModel;
import de.flixbus.presentation.model.TimeTableViewModel;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.mock;

/*
 * Created by emanuele on 02.07.16.
 */
public class TimeTableModelMapperTest {


    private TimeTableMapper mTimeTableMapper;

    @Before
    public void setup() {
        mTimeTableMapper = new TimeTableMapper();
    }


    @Test
    public void testTimeTableMapper() {
        TimeTable timeTable = EntityUtils.createTimeTable();
        assertThat(timeTable, is(notNullValue()));
        TimeTableViewModel timeTableViewModel = mTimeTableMapper.transform(timeTable);
        assertThat(timeTableViewModel, is(instanceOf(TimeTableViewModel.class)));
        assertThat(timeTableViewModel, is(notNullValue()));

        assertThat(timeTableViewModel.getArrivals(), is(notNullValue()));
        assertThat(timeTableViewModel.getArrivals().size(), is(EntityUtils.ROUTES_NUMBER));

        assertThat(timeTableViewModel.getDepartures(), is(notNullValue()));
        assertThat(timeTableViewModel.getDepartures().size(), is(EntityUtils.ROUTES_NUMBER));

        assertThat(timeTableViewModel.getStation(), is(notNullValue()));
        assertThat(timeTableViewModel.getStation(), is(instanceOf(StationViewModel.class)));
        assertThat(timeTableViewModel.getStation().getAddress(), is(equalTo(EntityUtils.FAKE_STATION_ADDRESS)));
        assertThat(timeTableViewModel.getStation().getName(), is(equalTo(EntityUtils.FAKE_STATION_NAME)));
        assertThat(timeTableViewModel.getStation().getId(), is(equalTo(EntityUtils.FAKE_STATION_ID)));
    }


    @Test
    public void testTimeTableCollectionMapper() {
        TimeTable timeTable = mock(TimeTable.class);
        TimeTable timeTable2 = mock(TimeTable.class);

        List<TimeTable> timeTableList = new ArrayList<>();
        timeTableList.add(timeTable);
        timeTableList.add(timeTable2);

        Collection<TimeTableViewModel> timeTableViewModelList = mTimeTableMapper.transform(timeTableList);
        assertThat(timeTableViewModelList, is(notNullValue()));
        assertThat(timeTableViewModelList.size(), is(equalTo(2)));
        assertThat(timeTableViewModelList.toArray()[0], is(instanceOf(TimeTableViewModel.class)));
        assertThat(timeTableViewModelList.toArray()[1], is(instanceOf(TimeTableViewModel.class)));

        timeTableList.clear();
        timeTableList.add(EntityUtils.createTimeTable());
        timeTableViewModelList = mTimeTableMapper.transform(timeTableList);

        assertThat(timeTableViewModelList, is(notNullValue()));
        assertThat(timeTableViewModelList.size(), is(equalTo(2)));
        assertThat(timeTableViewModelList.toArray()[0], is(instanceOf(TimeTableViewModel.class)));
        assertThat(timeTableViewModelList.toArray()[1], is(instanceOf(TimeTableViewModel.class)));

    }
}