package de.flixbus.data.entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import de.flixbus.data.net.TimeTableDeserializer;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by emanuele on 27.06.16.
 */
public class TimeTableEntityJsonTest {
    private static final String JSON_TIME_TABLE = "{\"timetable\": " +
            "{ " +
            "\"arrivals\": [ {}, {}, {} ],  " +
            "\"departures\": [ {}, {} ],  " +
            "\"message\": \"" + EntityUtils.FAKE_MESSAGE + "\" " +
            "}," +
            "\"station\": { \"id\": " + EntityUtils.FAKE_STATION_ID + " } " +
            "}";

    private Gson mGson;

    public TimeTableEntityJsonTest() {
        mGson = new GsonBuilder().registerTypeAdapter(TimeTable.class, new TimeTableDeserializer()).create();
    }

    @Rule
    public ExpectedException mExpectedException = ExpectedException.none();

    @Test
    public void testGsonParsingTimeTable() {
        TimeTable timeTable = mGson.fromJson(JSON_TIME_TABLE, TimeTable.class);
        assertThat(timeTable, is(notNullValue()));
        assertThat(timeTable.getArrivals().size(), is(3));
        assertThat(timeTable.getDepartures().size(), is(2));
        assertThat(timeTable.getStation(), is(notNullValue()));
        assertThat(timeTable.getStation().getId(), is(equalTo(EntityUtils.FAKE_STATION_ID)));
        assertThat(timeTable.getStation().getAddress(), is(nullValue()));
        assertThat(timeTable.getMessage(), is(equalTo(EntityUtils.FAKE_MESSAGE)));

    }
}
