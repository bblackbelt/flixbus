package de.flixbus.data.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by emanuele on 02.07.16.
 */
public class EntityUtils {

    public static final int FAKE_STATION_ID = 123;
    public static final String FAKE_STATION_NAME = "My Station";
    public static final String FAKE_STATION_ADDRESS = "My Address";
    public static final String FAKE_STATION_FULL_ADDRESS = "My Address";
    public static final String FAKE_MESSAGE = "My Message";
    public static final String LINE_CODE = "123";
    public static final String LINE_DIRECTION = "123 direction";
    public static final String DIRECTION = "DIRECTION";


    public static final long FAKE_TIMESTAMP = System.currentTimeMillis();
    public static final String FAKE_TZ = "UTC";

    public static final String FAKE_THROUGH_THE_STATIONS = "A B C";
    public static final int ROUTES_NUMBER = 4;

    public static List<Route> createRoutes(int num) {
        List<Route> routes = new ArrayList<>(num);
        for (int i = 0; i < num; i++) {
            Route route = new Route();
            route.setAddress(FAKE_STATION_ADDRESS);
            route.setId(FAKE_STATION_ID);
            route.setName(FAKE_STATION_NAME);
            routes.add(route);
        }
        return routes;
    }

    public static List<Trip> createTrip(int num) {
        List<Trip> trips = new ArrayList<>(num);
        for (int i = 0; i < num; i++) {
            Trip trip = new Trip();
            trip.setThroughTheStations(FAKE_THROUGH_THE_STATIONS);
            trip.setLineCode(LINE_CODE);
            trip.setLineDirection(LINE_DIRECTION);
            trip.setDirection(DIRECTION);
            trip.setRoute(createRoutes(ROUTES_NUMBER));

            Trip.DateTime dateTime = new Trip.DateTime();
            dateTime.setTimeStamp(i > num / 2 ? FAKE_TIMESTAMP : getTomorrow());
            dateTime.setTz(FAKE_TZ);
            trip.setDatetime(dateTime);

            trips.add(trip);
        }
        return trips;
    }

    public static TimeTable createTimeTable() {
        TimeTable timeTable = new TimeTable();
        Station station = new Station();
        station.setFullAddress(FAKE_STATION_FULL_ADDRESS);
        station.setId(FAKE_STATION_ID);
        station.setName(FAKE_STATION_NAME);
        station.setAddress(FAKE_STATION_ADDRESS);
        timeTable.setStation(station);
        timeTable.setArrivals(createTrip(ROUTES_NUMBER));
        timeTable.setDepartures(createTrip(ROUTES_NUMBER));
        return timeTable;
    }

    private static long getTomorrow() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTimeInMillis();
    }

    private EntityUtils() {
    }
}
