package de.flixbus.data.interactor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import de.flixbus.core.RxSchedulerWrapper;
import de.flixbus.data.entity.TimeTable;
import rx.Observable;
import rx.observers.TestSubscriber;
import rx.schedulers.TestScheduler;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;

/**
 * Created by emanuele on 02.07.16.
 */
public class TimeTableInteractorTest {


    private class TimeTableTestInteractor extends Interactor<TimeTable> {

        public TimeTableTestInteractor(RxSchedulerWrapper schedulerWrapper) {
            super(schedulerWrapper);
        }

        @Override
        protected Observable<TimeTable> buildInteractorObservable() {
            return Observable.just(new TimeTable());
        }
    }


    private TimeTableTestInteractor mTimeTableTestInteractor;

    @Mock
    private RxSchedulerWrapper mSchedulerWrapper;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mTimeTableTestInteractor = new TimeTableTestInteractor(mSchedulerWrapper);
    }

    @Test
    public void testBuildInteractorObservableReturnCorrectResult() {
        final TestSubscriber<TimeTable> testSubscriber = new TestSubscriber<>();
        final TestScheduler testScheduler = new TestScheduler();
        given(mSchedulerWrapper.getSubscribeOn()).willReturn(testScheduler);
        given(mSchedulerWrapper.getObserveOn()).willReturn(testScheduler);
        mTimeTableTestInteractor.execute(testSubscriber);
        testScheduler.triggerActions();
        testSubscriber.assertNoErrors();
        assertThat(testSubscriber.getOnNextEvents().isEmpty(), is(false));
        assertThat(testSubscriber.getOnNextEvents().size(), is(1));
        assertThat(testSubscriber.getOnNextEvents().get(0), is(instanceOf(TimeTable.class)));
    }


    @Test
    public void testInterarctorUnsubscribe() {
        TestSubscriber<TimeTable> testSubscriber = new TestSubscriber<>();
        mTimeTableTestInteractor.execute(testSubscriber);
        mTimeTableTestInteractor.unsubscribe();
        assertThat(testSubscriber.isUnsubscribed(), is(true));
    }

    @After
    public void tearDown() {
        mTimeTableTestInteractor.unsubscribe();
    }

}
